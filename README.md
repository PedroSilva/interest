# Interest Calc


This is nothing more than a plain simple interest calculator to be used in the command line.
I've simply created this mini project as a tool to reason about small investments such as bonds or savings accounts. It supports the bare minimum: 
- Fixed interest rate;
- Frequency definition in months or years;
- Number of periods;
- Interest tax.

Although it is very simple it is handy and I use it now and then. Maybe someone else finds it useful as well.

## Install

With pip install

```
pip install interest-calc
```

# Help content
```
Calculate the interest over time for a given deposit, and interest rate.
Can set the tax rate in order to calculate actual gains.

Usage:
  interest-calc [-h | --help]
  interest-calc <deposit> <interest_rate> <duration> [--p-size=<period_size>] [--tax=<tax_rate>]

Options:
  -h --help                                     Show this help message and exit.
  <deposit>                                     Initial deposit value (investment).
  <interest>                                    Interest rate on the deposit.
  <duration>                                    Duration of the deposit (default in years).
  -p <period_size> --p-size=<period_size>]      Redefine the duration size in months.
  -t <tax_rate> --tax=<tax_rate>                Set tax rate to compute actual gains.
```

# Examples

A simple example of a 10000 units deposit with 4% interest over 5 periods. This includes no tax and the default period is one year.
```
pedro-silva: interest-calc EssentiaLaptop2$ interest-calc 10000 .04 5
Initial deposit of 10000.0 over 5 periods of 12 months generate the following gains:

After 12 months deposit value is 10400.0; gain of 4.0 %.
After 24 months deposit value is 10816.0; gain of 8.16 %.
After 36 months deposit value is 11248.64; gain of 12.486 %.
After 48 months deposit value is 11698.586; gain of 16.986 %.
After 60 months deposit value is 12166.529; gain of 21.665 %.
```

Here is another example but uses 10 periods of 6 months instead, and considers a tax of 20% over the interest.
```
pedro-silva: interest-calc 10000 .04 10 -p 6 -t .2
Initial deposit of 10000.0 over 10 periods of 6 months generate the following gains:

After 6 months deposit value is 10160.0; gain of 1.6 %.
After 12 months deposit value is 10322.56; gain of 3.226 %.
After 18 months deposit value is 10487.721; gain of 4.877 %.
After 24 months deposit value is 10655.524; gain of 6.555 %.
After 30 months deposit value is 10826.013; gain of 8.26 %.
After 36 months deposit value is 10999.229; gain of 9.992 %.
After 42 months deposit value is 11175.217; gain of 11.752 %.
After 48 months deposit value is 11354.02; gain of 13.54 %.
After 54 months deposit value is 11535.685; gain of 15.357 %.
After 60 months deposit value is 11720.256; gain of 17.203 %.
```
